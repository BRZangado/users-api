DROP DATABASE IF EXISTS platform;
CREATE DATABASE platform;
GRANT ALL PRIVILEGES ON platform.* TO 'platform'@'%' IDENTIFIED BY 'platform';
GRANT ALL PRIVILEGES ON platform.* TO 'platform'@'localhost' IDENTIFIED BY 'platform';