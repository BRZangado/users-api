package com.platformbuilders.dto;

import lombok.Data;

@Data
public class UserRespondeDto extends UserCreationDto {
    private long id;
    private int age;
}
