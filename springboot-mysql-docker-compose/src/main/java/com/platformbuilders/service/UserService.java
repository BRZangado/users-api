package com.platformbuilders.service;

import com.platformbuilders.dto.UserCreationDto;
import com.platformbuilders.dto.UserRespondeDto;
import com.platformbuilders.exception.UserExceptions;
import com.platformbuilders.model.User;
import com.platformbuilders.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public List<UserRespondeDto> getUsers(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<User> pagedResult = repository.findAll(paging);
        if(pagedResult.hasContent()) {
            List<User> users = pagedResult.getContent();

            return users.stream().map(user -> {
                return mapToDto(user);
            }).collect(Collectors.toList());
        }

        return new ArrayList<UserRespondeDto>();
    }

    public UserRespondeDto getUser(String cpf) throws UserExceptions.UserNotFoundException {
        Optional<User> userOptional = repository.findByCpf(cpf);

        if(userOptional.isPresent()) {
            UserRespondeDto user = mapToDto(userOptional.get());
            return user;
        }

        throw new UserExceptions.UserNotFoundException();
    }

    public User saveUserDto(UserCreationDto dto){
        return repository.save(mapToModel(dto));
    }

    public User updateUser(User userinfo) throws UserExceptions.CannotUpdateUserException {
        Optional<User> optionalUser = repository.findById(userinfo.getId());
        if(optionalUser.isPresent() && (!userinfo.getName().isEmpty() || !userinfo.getCpf().isEmpty() || userinfo.getBirthday() != null)) {
            User user = optionalUser.get();
            if(!userinfo.getName().isEmpty()) user.setName(userinfo.getName());
            if(!userinfo.getCpf().isEmpty()) user.setCpf(userinfo.getCpf());
            if(userinfo.getBirthday() != null) user.setBirthday(userinfo.getBirthday());

            repository.save(user);
        }

        throw new UserExceptions.CannotUpdateUserException();
    }

    public void deleteUser(long id) throws UserExceptions.UserNotFoundException {
        Optional<User> user = repository.findById(id);
        user.ifPresent(value -> repository.delete(value));

        throw new UserExceptions.UserNotFoundException();
    }

    private UserRespondeDto mapToDto(User user){
        UserRespondeDto dto = new UserRespondeDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setCpf(user.getCpf());
        dto.setBirthday(user.getBirthday());
        dto.setAge(calculateAge(user.getBirthday()));
        return dto;
    }

    private User mapToModel(UserCreationDto dto){
        User user = new User();
        user.setName(dto.getName());
        user.setCpf(dto.getCpf());
        user.setBirthday(dto.getBirthday());
        return user;
    }

    public long getTotal(){
        return repository.count();
    }

    public static int calculateAge(LocalDate birthDate) {
        if (birthDate != null) {
            return Period.between(birthDate, LocalDate.now()).getYears();
        } else {
            return 0;
        }
    }
}
