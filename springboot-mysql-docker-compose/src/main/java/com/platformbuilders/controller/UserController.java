package com.platformbuilders.controller;

import com.platformbuilders.dto.UserCreationDto;
import com.platformbuilders.dto.UserRespondeDto;
import com.platformbuilders.exception.UserExceptions;
import com.platformbuilders.model.User;
import com.platformbuilders.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(UserController.MAPPING_BASE_URL)
@Api(value = "USER API Endpoints", produces = "application/json")
public class UserController {

    public static final String MAPPING_BASE_URL = "/api/v1/users";

    @Autowired
    private UserService service;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Users",
            httpMethod = "GET")
    public ResponseEntity<List<UserRespondeDto>> index(@RequestParam(defaultValue = "0") Integer page,
                                            @RequestParam(defaultValue = "10") Integer size,
                                            @RequestParam(defaultValue = "id") String sortBy) {

        List<UserRespondeDto> list = service.getUsers(page, size, sortBy);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        headers.add("count", String.valueOf(service.getTotal()));

        return ResponseEntity.ok().headers(headers).body(list);
    }

    @GetMapping(path = "/{cpf}/", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get User by CPF",
            httpMethod = "GET")
    public ResponseEntity<UserRespondeDto> fetch(@PathVariable() String cpf) {

        try {
            UserRespondeDto user = service.getUser(cpf);
            return ResponseEntity.ok().body(user);
        } catch (UserExceptions.UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new user",
            httpMethod = "POST",
            response = User.class)
    public User send(@RequestBody UserCreationDto dto) {
        return service.saveUserDto(dto);
    }

    @DeleteMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete user by id",
            httpMethod = "DELETE",
            response = User.class)
    public ResponseEntity<?> delete(@RequestParam() long id) {
        try {
            service.deleteUser(id);
            return ResponseEntity.ok().build();
        } catch (UserExceptions.UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update user",
            httpMethod = "PUT",
            response = User.class)
    public ResponseEntity<User> update(@RequestBody() User dto) {
        try {
            User user = service.updateUser(dto);
            return ResponseEntity.ok().body(user);
        } catch (UserExceptions.CannotUpdateUserException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
