import com.platformbuilders.controller.UserController;
import com.platformbuilders.dto.UserRespondeDto;
import com.platformbuilders.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {UserController.class})
public class UserControllerTest {

    @InjectMocks
    private UserController userController;
    @Mock
    private UserService service;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    private List<UserRespondeDto> generateUsersResponseDtpMockData(){
        List<UserRespondeDto> dtos = new ArrayList<>();
        UserRespondeDto dto = new UserRespondeDto();
        dto.setName("testName");
        dto.setCpf("testCpf");
        dtos.add(dto);
        return dtos;
    }

    @Test
    public void shouldReturnUsersList() throws Exception {

        List<UserRespondeDto> respondeDtos = generateUsersResponseDtpMockData();
        when(this.service.getUsers(anyInt(), anyInt(), anyString())).thenReturn(respondeDtos);
        when(this.service.getTotal()).thenReturn((long) 1);

        this.mockMvc
                .perform(get(UserController.MAPPING_BASE_URL))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(
                        jsonPath(
                                "$[*].name",
                                containsInAnyOrder(respondeDtos.stream().map(w -> w.getName()).toArray(String[]::new))))
                .andExpect(
                        jsonPath(
                                "$[*].cpf",
                                containsInAnyOrder(respondeDtos.stream().map(w -> w.getCpf()).toArray(String[]::new))));
    }
}
