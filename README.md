# USERS SERVICE - PLATFORM BUILDERS TEST

### INTRODUCTION

A REST API for USERS CRUD.


This project uses:  
* Spring Boot  
* MAVEN  
* Docker  
* Docker Compose  
* Swagger for API documentation  

### TO RUN
You will need docker and docker compose  
Access the main folder and run ``sudo docker-compose up``  
obs.: first time running takes some time to download dependencies. After that, build will be fast

Access http://localhost:8080/swagger-ui.html to consume endpoints by documentation.

### TODO
Improve spring app Dockerfile  
Push image to docker hub  
Improve tests

